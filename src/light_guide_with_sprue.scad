/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2019 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <light_guide.scad>;

/**
 * A light guide with a sprue ("Anguss") to get it ready for many at once
 */
module light_guide_with_sprue(diameter, width, height, sprue_thickness, sprue_length) {
  cube_lenght = 1;
  minimal_distance = 0.3;
  minimal_spure_thickness = 0.6;

  // Check if minimal sprue is given
  minimal_sprue_thickness_message = str("Sprue thickness to small! Minimum ", minimal_spure_thickness);
  assert(sprue_thickness >= minimal_spure_thickness, minimal_sprue_thickness_message);


  // Check if space between sprue and disk is kept
  message = str("Sprue thickness to big! Maximum is ", cube_lenght - minimal_distance);
  assert(sprue_thickness + minimal_distance <= cube_lenght, message);

  union() {
    light_guide(diameter, width, height, cube_lenght);
    _sprue();
  }

  module _sprue() {
    // Colors it white, slightly transparent (as "Fine Detail Plastic" from Shapeways)
    color("White", 0.8) translate([sprue_length / 2 + width / 2, 0, sprue_thickness / 2]) {
      cube([sprue_length, sprue_thickness, sprue_thickness], true);
    }
  }
};

light_guide_with_sprue(diameter = 1.8, width = 1.4, height = 1.1, sprue_thickness = 0.6, sprue_length = 1.5, $fn = 50);
