/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * An oversimplified model of the MD-44's Electro-Magnetic Rail Brake
 * made by Roco.
 *
 * Sources:
 * [1] Own measurement at the Roco model
 */
module emrb_with_holder_model() {
  vertical_offset = 1.3; // Taken from [1]
  overall_thickness = 2.3; // Taken from [1]
  holder_base_height = 3.55; // Taken from [1]

  union() {
    translate([0, overall_thickness, -holder_base_height + vertical_offset]) _emrb();
    _holder(holder_base_height);
  }

  // Oversimplified!
  module _holder(base_height) {
    length = 6.4; // Taken from [1]
    thickness = 1.55; // Taken from [1]
    pin_width = 3.2; // Taken from [1]
    pin_height = 2.2; // Taken from [1]

    translate([0, thickness / 2, 0]) union() {
      // Base
      translate([0, 0, -base_height / 2]) cube([length, thickness, base_height], true);
      // Pin
      translate([0, 0, pin_height / 2]) cube([pin_width, thickness, pin_height], true);
    }
  }

  // Oversimplified!
  module _emrb() {
    length = 14.8; // Taken from [1]
    thickness = 1.3; // Taken from [1]
    height = 3.2; // Taken from [1]

    translate([0, -thickness / 2, -height / 2]) cube([length, thickness, height], true);
  }
}

emrb_with_holder_model();
