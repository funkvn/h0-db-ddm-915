/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <light_guide_with_sprue.scad>;

/**
 * Two times light guide, connected by sprue
 */
module light_guide_2x(diameter, width, height) {
  part_grid = 3;
  sprue_thickness = 0.7;
  sprue_length = (part_grid - width) / 2;
  central_sprue_height = 1.5;

  translate([0, 0, central_sprue_height]) union() {
    _positioned_light_guide_with_sprue();
    mirror([1, 0, 0]) _positioned_light_guide_with_sprue();
    _central_sprue();
  }

  module _positioned_light_guide_with_sprue() {
    translate([-part_grid / 2, 0, 0]) light_guide_with_sprue(diameter, width, height, sprue_thickness, sprue_length);
  }

  module _central_sprue() {
    // Colors it white, slightly transparent (as "Fine Detail Plastic" from Shapeways)
    color("White", 0.8)  {
      translate([0, 0, -central_sprue_height / 2]) cube([sprue_thickness, sprue_thickness, central_sprue_height], true);
    }
  }
}

light_guide_2x(diameter = 1.8, width = 1.4, height = 1.1, $fn = 200);
