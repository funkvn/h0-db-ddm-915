/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2019 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <bogie_model.scad>;
use <bogie_connector.scad>;

/**
 * A holder for a special lightning board or a decoder to provide a power
 * supply for the rear light
 *
 * Sources:
 * [1] Own measurement at the Roco model
 * [2] Own measurement at the lighting board from Budelmann
 */
module board_holder(board_length, board_width, board_thickness, emrb = true) {

  base_width = 21.5; // Taken from [1]
  base_length = 14.7; // Taken from [1]
  connector_diameter = 4.9; // Taken from [1]
  emrb_thickness = 2.3; // Taken from [1]

  // Set the values for the part itself
  additional_space = 0.1; // To get the board inserted
  material_thickness = 0.6;
  girder_height = 1.8;
  girder_thickness_minimum = 1.2;
  distance_crosswise_girder = 6.5; // Placed aside the Electro-Magnetic Rail Brake center part

  // Values from the board dimensions
  space_vertical = board_thickness + additional_space;
  space_lengthwise = board_length + additional_space;
  space_crosswise = board_width + additional_space;

  //%bogie_model(emrb);
  %bogie_connector(1.5);

  board_translate_y = -base_width / 2 + emrb_thickness;
  %translate([0, board_translate_y, 0]) _board();

  color("White", 0.8) union() {
    // Place this in the middle of the board
    translate([0, board_translate_y + board_width / 2, 0]) _lengthwise_girder();

    translate_x = (distance_crosswise_girder + material_thickness) / 2;
    translate([translate_x, 0, 0]) _crosswise_girder();
    translate([-translate_x, 0, 0]) _crosswise_girder();

    // Place the crosswise girder connection under the board
    translate([0, board_translate_y + material_thickness / 2  , 0]) _crosswise_girder_connection();

    if (!emrb) {
      translate([0, board_translate_y - board_thickness, -girder_height]) _board_spacer();
    }
  }

  module _crosswise_girder_connection() {
    translate([0, 0, -girder_height / 2]) cube([distance_crosswise_girder, material_thickness, girder_height], true);
  }

  module _lengthwise_girder() {
    rotate(-90, [1, 0, 0]) linear_extrude(material_thickness, center = true) _lengthwise_girder_shape();
  }

  module _lengthwise_girder_shape() {
    assert((base_length - space_lengthwise) / 2 >= girder_thickness_minimum, "Error: board is to long!");

    girder_thickness = girder_thickness_minimum;
    length = space_lengthwise + 2 * girder_thickness;
    points = [
      [0, 0],
      [length, 0],
      [length, girder_height + space_vertical + material_thickness],
      [length - girder_thickness, girder_height + space_vertical + material_thickness],
      //[length - girder_thickness - material_thickness, girder_height + space_vertical],
      [length - girder_thickness, girder_height + space_vertical],
      [length - girder_thickness, girder_height],
      [girder_thickness, girder_height],
      [girder_thickness, girder_height + space_vertical],
      //[girder_thickness + material_thickness, girder_height + space_vertical],
      [girder_thickness, girder_height + space_vertical + material_thickness],
      [0, girder_height + space_vertical + material_thickness],
    ];
    // Clip translation
    translate_y = girder_height + space_vertical + material_thickness / 2;
    translate([-length / 2 , 0]) union() {
      translate([girder_thickness, translate_y]) circle(d = material_thickness);
      translate([length - girder_thickness, translate_y]) circle(d = material_thickness);
      polygon(points = points);
    }
  }

  module _crosswise_girder() {
    translate([0, -base_width / 2, 0]) rotate(90, [0, 0, 1]) rotate(-90, [1, 0, 0]) linear_extrude(material_thickness, center = true) _crosswise_girder_shape();
  }

  module _crosswise_girder_shape() {
    girder_thickness = (base_length - space_crosswise) / 2;
    points = [
      [0, 0],
      [space_crosswise + emrb_thickness + girder_thickness, 0],
      [space_crosswise + emrb_thickness + girder_thickness, girder_height + space_vertical + material_thickness],
      [space_crosswise + emrb_thickness, girder_height + space_vertical + material_thickness],
      //[space_crosswise + emrb_thickness - material_thickness, girder_height + space_vertical],
      [space_crosswise + emrb_thickness, girder_height + space_vertical],
      [space_crosswise + emrb_thickness, girder_height],
      [0, girder_height],
    ];
    // Clip translation
    translate_y = girder_height + space_vertical + material_thickness / 2;
    union() {
      translate([space_crosswise + emrb_thickness, translate_y]) circle(d = material_thickness);
      polygon(points = points);
    }
  }

  module _board_spacer() {
      assert(emrb_thickness >= board_thickness, "Error: no required space for board spacer!");
      translate([0, board_thickness / 2, -board_thickness / 2]) cube([distance_crosswise_girder + 2 * material_thickness, board_thickness, board_thickness], true);
  }

  module _board() {
    color("Green") translate([0, board_width / 2, -board_thickness / 2 - girder_height]) cube([board_length, board_width, board_thickness], true);
  }
}

// "Budelmann'sche Beleuchtungsplatine"
// Values taken from the grounded board
board_length = 12.0; // Taken from [2]
board_width = 10.0; // Taken from [2]
board_thickness = 1.55; // Taken from [2]

board_holder(board_length, board_width, board_thickness, true);
