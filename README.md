# DB DDm 915 &ndash; beleuchtetes Schlusslicht (H0)

Für das Roco-Modell eines Autoreisezugwagens der Bauart DDm 915
der Deutschen Bundesbahn.

Teile für ein funktionsfähiges Schlusslicht (zwei rote Lichter, Zg 2).

## Beschreibung

Zum einen gibt es einen Platinen-Halter, der in das Drehgestell geklebt wird,
um dort einen kleinen Decoder oder eine Budelmann'sche Beleuchtungsplatine zu
befestigen. Der Platz dort reicht aus, um das unsichtbar zu relaisieren.

Außerdem gibt es eine Art kurzen Lichtleiter, der ansatt der aufgedruckten
Schlusslichter (einer Seite) eingebaut wird. An den Lichtleiter wird eine
LED geklebt, die geschickt durch den Wagen, fast komplett unsichtbar mit der
Platine verkabelt wird.
