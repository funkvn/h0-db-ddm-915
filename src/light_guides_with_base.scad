/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <base.scad>;
use <light_guide_2x.scad>;

/**
 * Many two-times-light-guides on a base plate
 */
module light_guides_with_base() {
  distance_x = 6;
  distance_y = 3;
  union() {
    base();
    for (i = [-2:2]) {
      for (j = [-1:1]) {
        translate([j * distance_x, i * distance_y, 0]) light_guide_2x(diameter = 1.8, width = 1.4, height = 1.1);
      }
    }
  }
}

light_guides_with_base($fn = 400);
