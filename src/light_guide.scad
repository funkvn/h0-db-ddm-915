/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2019 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Light guide for Roco H0 model
 * Lichtleiter für das H0-Roco-Modell
 *
 * Developed for a wired 0402 LED.
 * Entwickelt für eine bedrahtete 0402-LED.
 *
 * Ressources/Quellen
 * [1]: Measures taken from Roco H0 model
 */
module light_guide(diameter, width, height, cube_length = 1) {
  // Check if the cube is full behind the plate
  hypotenuse = sqrt(pow(width, 2) + pow(height, 2));
  message = str("Diameter to small! It has to be at least ", hypotenuse);
  assert(hypotenuse <= diameter, message);

  length = 2;

  // LED 0402 dimensions
  led_length = 0.9;
  led_width = 0.5;
  led_height = 0.4;
  led_board_thickness = 0.1;
  led_space = 0.12;

  // Show LED only in preview
  %_led(led_length, led_width, led_height, led_board_thickness);

  // Colors it white, slightly transparent (as "Fine Detail Plastic" from Shapeways)
  color("White", 0.8) union() {
    // Round part, the "Schlusslicht" itself
    translate([0, 0, cube_length]) cylinder(h = length - cube_length, d = diameter, true);
    // Cubic part with deepening for LED
    difference() {
      translate([0, 0, cube_length / 2]) cube([width, height, cube_length], true);
      // We add some space (width, length) to let fit in the LED.
      // To ensure that the LED touches the light guide we do not
      // deep to the full height of the LED.
      translate([0, 0, led_space]) cube([led_length + led_space, led_width + led_space, led_height], true);
    }
  }

  module _led(length, width, height, board_thickness) {
    union() {
      translate([0, 0, height / 2]) color("LightYellow") cube([length, width, height], true);
      translate([0, 0, -board_thickness / 2]) color("Silver") cube([length + 2 * board_thickness, width, board_thickness], true);
    }
  }
}

// Values taken from [1]
light_guide(1.8, 1.4, 1.1, $fn = 50);
