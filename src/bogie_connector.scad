/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

$fn = 50;

/**
 * The modified connector to combine bogie and motorail car
 *
 * Modification: cut the pin on the opposite side of the plate.
 *
 * Sources:
 * [1] Own measurement at the Roco model
 */
module bogie_connector(base_thickness) {
  diameter_1 = 4.9; // Taken from [1]
  diameter_2 = 3.05; // Taken from [1]
  diameter_3 = 2.0; // Taken from [1]
  diameter_4 = 1.8; // Taken from [1]
  head_thickness = 1.4; // Taken from [1]
  height_to_2 = 3.25;
  height_to_3 = 5.5;
  height_to_4 = 6.4;

  // Calculate the space between base and connector head
  height_2 = height_to_2 - head_thickness;
  space = height_2 - base_thickness;
  if (is_num(base_thickness)) {
    assert(space > 0, "Base thickness is to large!");
  }

  // Shift to the space between base and connector head if thickness is given
  translate_z = is_num(base_thickness) ? -head_thickness - space: -head_thickness;

  color("Dimgray") translate([0, 0, translate_z]) union() {
    cylinder(h = head_thickness, d = diameter_1);
    cylinder(h = height_to_2, d = diameter_2);
    cylinder(h = height_to_3, d = diameter_3);
    cylinder(h = height_to_4, d = diameter_4);
  }
}

bogie_connector();
