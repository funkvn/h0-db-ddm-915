/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use <emrb_with_holder_model.scad>;

/**
 * An oversimplified model of the MD-44 bogie frame, made by Roco
 *
 * Sources:
 * [1] Own measurement at the Roco model
 */
module bogie_model(emrb = true) {
  base_thickness = 1.5; // Taken from [1], somewhat precice
  inner_width = 21.5; // Taken from [1]
  lateral_opening_length = 3.2; // Taken from [1], somewhat precice
  outer_beams_thickness = 2.2; // Taken from [1]
  outer_beams_height = 2.5; // Taken from [1]
  outer_beams_vertical_offset = 1.15; // Taken from [1]

  color("Dimgray") union() {
    _base();
    // Outer beams
    _outer_beams();
    mirror([0, 1, 0]) _outer_beams();
    if (emrb) {
      // EMRB with holder
      translate([0, -inner_width / 2, 0]) emrb_with_holder_model();
      mirror([0, 1, 0]) translate([0, -inner_width / 2, 0]) emrb_with_holder_model();
    }
  }

  module _base() {
    width = inner_width;
    length = 14.9; // Taken from [1]
    lateral_opening_width = 1.7; // Taken from [1], somewhat precice

    translate([0, 0, base_thickness / 2]) difference() {
      cube([length, width, base_thickness], true);
      _center_opening();
      translate([0, inner_width / 2 - lateral_opening_width / 2, 0]) _lateral_opening();
      translate([0, -inner_width / 2 + lateral_opening_width / 2, 0]) _lateral_opening();
    }

    module _center_opening() {
      center_opening_diameter = 3.2; // Taken from [1], somewhat precice
      cylinder(h = base_thickness * 1.1, d = center_opening_diameter, center = true);
    }

    module _lateral_opening() {
      cube([lateral_opening_length, lateral_opening_width, base_thickness * 1.1], true);
    }
  }

  // Oversimplified!
  module _outer_beams() {
    length = 48.2; // Taken from [1]
    center_part_length = 13.5; // Taken from [1]
    center_part_height = 7.5; // Taken from [1]
    translate_z = -outer_beams_height + outer_beams_vertical_offset + base_thickness;

    translate([0, inner_width / 2 + outer_beams_thickness / 2, translate_z]) union() {
      // Outer beams itself
      translate([0, 0, outer_beams_height / 2]) cube([length, outer_beams_thickness, outer_beams_height], true);
      // Center part
      translate([0, 0, -outer_beams_height / 2]) cube([center_part_length, outer_beams_thickness, center_part_height], true);
    }
  }
}

bogie_model($fn = 50);
